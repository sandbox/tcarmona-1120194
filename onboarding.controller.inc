<?php

class OnboardingController extends DrupalDefaultEntityController{

  public function save($onboarding) {
    drupal_write_record('onboarding', $onboarding);
    field_attach_insert('onboarding', $onboarding);
    module_invoke_all('entity_insert', 'onboarding', $onboarding);
    return $onboarding;
  }

}
